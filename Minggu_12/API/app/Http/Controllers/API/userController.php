<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class userController extends Controller
{
    public function getAll()
    {
        $user = User::get();
        return response()->json(['msg' => 'Data Berhasil ditemukan', 'data' => $user], 200);
    }


    public function getID($id)
    {
        $user = User::findOrFail($id);
        return response()->json(['msg' => 'Data data Berhasil ditemukan', 'data' => $user], 200);
    }

    public function createUser(Request $request)
    {
        $user = User::create($request->all());
        return response()->json(['msg' => 'Data data Berhasil ditambahkan', 'data' => $user], 201);
    }

    public function updateUser($id, Request $request)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return response()->json(['msg' => 'Data data Berhasil di update', 'data' => $user], 200);
    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(['msg' => 'Data data Berhasil di hapus'], 200);
    }
}
