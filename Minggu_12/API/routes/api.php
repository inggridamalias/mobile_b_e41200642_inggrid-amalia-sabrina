<?php

use App\Http\Controllers\API\barangController;
use App\Http\Controllers\API\userController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('userApi', [userController::class, 'getAll']);
Route::get('userApi/{id}', [userController::class, 'getID']);
Route::post('userApi', [userController::class, 'createUser']);
Route::put('userApi/{id}', [userController::class, 'updateUser']);
Route::delete('userApi/{id}', [userController::class, 'deleteUser']);
