import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './editdata.dart';
import './main.dart';

class Detail extends StatefulWidget {
  List list;
  int index;
  Detail({
    Key? key,
    required this.list,
    required this.index,
  }) : super(key: key);
  @override
  _DetailState createState() => new _DetailState();
}

class _DetailState extends State<Detail> {
  void deleteData() {
    var url = 'http://192.168.1.87:8080/api/userApi/';
    http.delete(Uri.parse(url + widget.list[widget.index]['id'].toString()),
        body: {
          'id': widget.list[widget.index]['id'].toString()
        }).then((response) {
      print('Response status:${response.statusCode}');
      print('Response body:${response.body}');
    });
  }

  void confirm() {
    AlertDialog alertDialog = new AlertDialog(
      content: new Text(
          "Are You sure want to delete '${widget.list[widget.index]['name']}'"),
      actions: <Widget>[
        new RaisedButton(
          child: new Text(
            "OK DELETE!",
            style: new TextStyle(color: Colors.black),
          ), // Text
          color: Colors.red,
          onPressed: () {
            deleteData();
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (BuildContext context) => new Home(),
            ));
          },
        ),
        new RaisedButton(
          child: new Text("CANCEL", style: new TextStyle(color: Colors.black)),
          color: Colors.green,
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
    showDialog(builder: (context) => alertDialog, context: context);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar:
          new AppBar(title: new Text("${widget.list[widget.index]['name']}")),
      body: new Container(
        height: 270.0,
        padding: const EdgeInsets.all(20.0),
        child: new Card(
          child: new Center(
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ), // Padding
                new Text(
                  "Nama:${widget.list[widget.index]['name']}",
                  style: new TextStyle(fontSize: 20.0),
                ), // Text
                new Text(
                  "Username:${widget.list[widget.index]['username']}",
                  style: new TextStyle(fontSize: 18.0),
                ),
                new Text(
                  "Email:${widget.list[widget.index]['email']}",
                  style: new TextStyle(fontSize: 18.0),
                ), // Text
                new Text(
                  "ID Akses: ${widget.list[widget.index]['id_akses']}",
                  style: new TextStyle(fontSize: 18.0),
                ), // Text
                new Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ), // Padding
                new Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new RaisedButton(
                      child: new Text("EDIT"),
                      color: Colors.green,
                      onPressed: () =>
                          Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new EditData(
                          list: widget.list,
                          index: widget.index,
                        ), // EditData
                      )), // MaterialPageRoute
                    ),
                    new RaisedButton(
                      child: new Text("DELETE"),
                      color: Colors.red,
                      onPressed: () => confirm(),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}// Scaffold