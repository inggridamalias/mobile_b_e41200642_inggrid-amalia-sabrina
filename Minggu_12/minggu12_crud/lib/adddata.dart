import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'main.dart';

class AddData extends StatefulWidget {
  @override
  _AddDataState createState() => new _AddDataState();
}

class _AddDataState extends State<AddData> {
  TextEditingController controllerNama = new TextEditingController();

  TextEditingController controllerUsername = new TextEditingController();

  TextEditingController controllerEmail = new TextEditingController();

  TextEditingController controllerPassword = new TextEditingController();

  TextEditingController controllerIdakses = new TextEditingController();

  void addData() {
    var url = "http://192.168.1.87:8080/api/userApi";
    http.post(Uri.parse(url), body: {
      "name": controllerNama.text,
      "username": controllerUsername.text,
      "email": controllerEmail.text,
      "password": controllerPassword.text,
      "id_akses": controllerIdakses.text,
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("ADD DATA"),
      ), // AppBar
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new TextField(
                  controller: controllerNama,
                  decoration:
                      new InputDecoration(hintText: "Nama", labelText: "Nama"),
                ), // TextField
                new TextField(
                  controller: controllerUsername,
                  decoration: new InputDecoration(
                      hintText: "Username", labelText: "Username"), // InputDe
                ),
                new TextField(
                  controller: controllerEmail,
                  decoration: new InputDecoration(
                      hintText: "Email", labelText: "Email"), // Inp
                ), // TextField
                new TextField(
                  controller: controllerPassword,
                  obscureText: true,
                  decoration: new InputDecoration(
                      hintText: "Password", labelText: "Password"), //
                ), // TextField
                new TextField(
                  controller: controllerIdakses,
                  decoration: new InputDecoration(
                      hintText: "ID Akses", labelText: "ID Akses"), //
                ), // TextField
                new Padding(
                  padding: const EdgeInsets.all(10.0),
                ), // Padding
// Padding
                new RaisedButton(
                  child: new Text("ADD DATA"),
                  color: Colors.blueAccent,
                  onPressed: () {
                    addData();
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (BuildContext context) => new Home())); // Mat
                  },
                ) // RaisedButton
              ],
            ), // Column
          ], // <Widget>[]
        ), // ListView
      ), // Padding
    ); // Scaffold
  }
}
