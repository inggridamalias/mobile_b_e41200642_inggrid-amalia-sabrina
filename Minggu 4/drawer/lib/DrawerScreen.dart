import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text("Inggrid Amalia Sabrina"),
          currentAccountPicture:
              CircleAvatar(backgroundImage: AssetImage("assets/images/me.jpg")),
          accountEmail: Text("inggridamalias99@gmail.com"),
        ),
        DrawerListTile(
          iconData: Icons.group_outlined,
          title: "New Group",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.person_outline,
          title: "Contacts",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.phone_outlined,
          title: "Calls",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.bookmark_border,
          title: "Saved Message",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.settings_outlined,
          title: "Settings",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.person_add_alt,
          title: "Invite Friends",
          onTilePressed: () {},
        ),
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
