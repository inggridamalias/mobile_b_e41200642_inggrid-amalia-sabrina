class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Mine',
      message: 'Love you',
      time: 'Just now',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7m.jpg'),
  ChartModel(
      name: 'Mark',
      message: 'Never regret anything that made you smile.',
      time: '23.59',
      profileUrl:
          'https://img.okezone.com/content/2019/04/30/298/2049787/chef-arnold-berbagi-tips-membuat-makanan-menjadi-lebih-enak-Tz2kVDuNL2.jpg'),
  ChartModel(
      name: 'Kyungsoo',
      message: 'Whatever you do, do it well.',
      time: '18.00',
      profileUrl:
          'https://asset.kompas.com/crops/ployX7cQOqsYqJS2PYvUGv41CaI=/0x0:1000x667/750x500/data/photo/2017/06/22/163146320170622-042902-8311-chef.juna-.atau-.junior-.rorimpandey-m.jpg'),
  ChartModel(
      name: 'Oliver',
      message: 'All limitations are self-imposed.',
      time: '15.33',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Jessi',
      message: 'Have enough courage to start and enough heart to finish.',
      time: 'Yesterday',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Niki',
      message: 'Yesterday you said tomorrow. Just do it.',
      time: 'Fri',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Curll',
      message: 'Wanting to be someone else is a waste of who you are.',
      time: 'Thu',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Ken',
      message: 'The time is always right to do what is right. ',
      time: 'Wed',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Habibi',
      message: 'White is not always light and black is not always dark.',
      time: 'Wed',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Opyy',
      message: 'Turn your wounds into wisdom.',
      time: 'Tue',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Buna',
      message: 'I will remember and recover, not forgive and forget.',
      time: 'Tue',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Chachaa',
      message: 'I have nothing to lose but something to gain.',
      time: 'Mon',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Me',
      message: 'Fightingg for yourself.',
      time: 'Mon',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Heidii',
      message: 'I have nothing to lose but something to gain.',
      time: 'Sun',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'Arish',
      message: 'Happiness depends upon ourselves.',
      time: 'Mar 12',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
];
